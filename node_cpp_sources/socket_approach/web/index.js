
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var path = require('path');
var router = express.Router();

var net = require('net');



var socket = new net.Socket();
socket.connect(8020, '127.0.0.1', function() {
	console.log('NodeJs socket connected');
});

socket.on('data', function(data) {
	console.log('NodeJs socket received: ' + data);
	//socket.destroy(); // kill socket after server's response
});

socket.on('close', function() {
	console.log('NodeJs socket closed');
});




app.use(express.static('public'));
app.set('view engine', 'jade');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var routes = [
    {
        title: "testroute",
        description: "Test if routing works properly, works as a base implementation."
    }
];

routes.forEach(function (route) {
    app.use('/'+route.title, require('./routes/' + route.title));
});

app.get('/',function(req,res)
{
    res.render('index', { title: "Node++", info:"placeholder", routes: routes });
});

app.post('/',function(req,res)
{
    console.log("Post function called, mymsg:");
    console.log(req.body.mymsg);

    socket.write(req.body.mymsg+"[\x00]");

    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({
        result: "Received: "+req.body.mymsg
    }));
});

module.exports = router;

var server = app.listen(8080, function () {
  console.log('Web server listing at http://localhost:%s', server.address().port);
});
