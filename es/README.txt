Az ES a TinyOS-hez hasonl� knyvt�r, C++ alapon. C�lja egys�ges modulrendszerrel
megoldani az �temez�s, kommunik�ci�, id�z�t�s, stb. probl�m�it.

A system mapp�ban tal�lhat�ak azok a modulok, amelyek fontosak a rendszer
m�k�d�se szempontj�b�l �s platformf�ggetlenek.

A platformf�gg� modulok a "platform/<platform name>" mapp�ban tal�lhat�ak.

Projekt l�trehoz�sakor a forr�sokat tartalmaz� mapp�ban sz�ks�ges a "system"
mappa, egy "platform" mappa, amely a "platform/<platform name>" tartalm�val
rendelkezik �s a felhaszn�l� �ltal hozz�adott modulok.

A felhaszn�l� programja �t kell, hogy adja a vez�rl�st az ES �temez�j�nek.