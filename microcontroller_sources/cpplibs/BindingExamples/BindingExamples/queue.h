#pragma once

#include <cstdint>
#include "es_base.h"

/*
	Class for interfacing between interruptable loops and non interruptable or one level interrupts.
	Interrupts and the loop side can only use separate ends of the queue. Thread safety applies in both directions.
	Size should be at least 2. Allocates Size+1 length array of T.
*/
template <typename T, uint8_t Size = 4>
class Queue {

	static const int N = Size + 1;

	uint8_t head = 0;
	uint8_t tail = 0; // strictly one byte to ensure atomic value assignment

	T q[N];

	inline uint8_t nextId(uint8_t id) const {
		++id;
		if (id == N) {
			id = 0;
		}
		return id;
	}

	void(*const signal)();

public:
	Queue() : signal([] {}) {}
	Queue(void(* const signal)()) : signal(signal){}

	// called by interrupt
	bool deposit(T item) {
		
		uint8_t nextTail = nextId(tail);
		if (nextTail != head) {

			bool signalNeeded = head == tail;

			q[tail] = item;
			tail = nextTail;

			if (signalNeeded){
				signal();
			}
			return true;
		} else {
			return false;
		}

	}

	// called by loop
	bool consume(T* const container) {
		// queue is empty
		if (head == tail) {
			return false;
		}

		*container = q[head];
		head = nextId(head);

		return true;
	}
};