// BindingExamples.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include <array>

#include "es_base.h"
#include "bufferqueue.h"



struct DAC0Registers{};

template<class Registers>
class DAC {
	DAC() = delete;
public:
	static bool refreshNeeded;
	static int outregister;

	static inline void signal() {
		if (refreshNeeded) {
			auto h = Consumer::dequeue();
			if (h.isValid()) {
				outregister = *h.buffer;
				refreshNeeded = false;
				Consumer::recycle(h);
			}
		}
	}

	CONSUMER_INTERFACE(Consumer,uint8_t)
};
template<class R>
bool DAC<R>::refreshNeeded;
template<class R>
int DAC<R>::outregister;

typedef DAC<DAC0Registers> DAC0;
typedef BufferQueue<uint8_t, 12> DAC0Queue;
BIND_CONSUMER(DAC0Queue::Consumer,DAC0::Consumer,uint8_t)
BIND_EVENT(DAC0,DAC0Queue::FirstItem)


void magnetometer() {
	std::cout << "----- Magnetometer -----" << std::endl;
	DAC0::refreshNeeded = true;
	DAC0::outregister = 0;

	std::cout << sizeof(DAC0Queue::Consumer) << std::endl;

	auto h = DAC0Queue::Producer().requestBuffer();
	if (h.isValid()) {
		*h.buffer = 123;
		DAC0Queue::Producer().enqueue(h);

		std::cout << "Magnetometer: " << DAC0::outregister << std::endl;
	} else {
		std::cout << "Magnetometer: " << "invalid handle" << std::endl;
	}
}








#include "frameprotocol\frame_streamer.h"
#include "frameprotocol\frame_assembler.h"

template<typename Owner = MainModule>
struct StreamPrinter {

	STREAM_INTERFACE(Stream,uint8_t)
	EVENT_INTERFACE(ACKEvent)

	struct DataAvailable {
		static void signal() {
			uint8_t byte;
			int cnt;

			while (true) {
				cnt = 0;
				std::cout << "Streamed data: ";
				while (Stream::fetch(&byte)) {
					std::cout << (char)byte;
					++cnt;
				}

				std::cout << std::endl;
				std::cout << "Number of bytes: " << cnt << std::endl;

				if (!cnt) {
					break;
				}

				ACKEvent::signal();
			}
		}
	};
};

BIND_STREAM(FrameStreamer<MainModule>::Stream, StreamPrinter<>::Stream, uint8_t)
BIND_EVENT(FrameStreamer<MainModule>::ACKEvent, StreamPrinter<>::ACKEvent)
BIND_EVENT(StreamPrinter<>::DataAvailable, FrameStreamer<MainModule>::HasData)

typedef BufferQueue<Frame, 10, FrameStreamer<MainModule> > BufferQueue_of_FrameStreamer;
BIND_CONSUMER(BufferQueue_of_FrameStreamer::Consumer, FrameStreamer<MainModule>::Consumer, Frame)
BIND_EVENT(FrameStreamer<MainModule>::FrameAvailableEvent,BufferQueue_of_FrameStreamer::FirstItem)

typedef BufferQueue_of_FrameStreamer SerialPortXYZ;


void framestreamer() {
	std::cout << "----- Frame streamer -----" << std::endl;
	
	for (int i = 0; i < 3; i++) {
		auto h = SerialPortXYZ::Producer::requestBuffer();

		if (h.isValid()) {
			h.buffer->reset();
			for (int i = 32; i < 127; i++) h.buffer->addByte(i);
		} else {
			std::cout << "Cannot get requested buffer" << std::endl;
		}

		std::cout << "Buffer enqueued" << std::endl;
		SerialPortXYZ::Producer::enqueue(h);
	}
}





template<typename Owner = MainModule>
struct Printer {

	STREAM_INTERFACE(Stream, uint8_t)

	struct DataAvailable {
		static void signal() {
			uint8_t byte;
			int cnt = 0;
			std::cout << "Streamed data: ";
			while (Stream::fetch(&byte)) {
				std::cout << (char)byte;
				++cnt;
			}

			std::cout << std::endl;
			std::cout << "Number of bytes: " << cnt << std::endl;
		}
	};
};

template<typename Owner = MainModule>
struct Source {

	EVENT_INTERFACE(HasData)

	static bool isAvailable;
	static uint8_t data;

	struct Stream {
		static inline bool fetch(uint8_t *container) {
			if (!isAvailable) return false;
			*container = data;
			return true;
		}
	};

	static void write(uint8_t d) {
		data = d;
		isAvailable = true;
		HasData::signal();
	}
};
template<typename Owner>
bool Source<Owner>::isAvailable;
template<typename Owner>
uint8_t Source<Owner>::data;



template<typename Owner = MainModule>
struct QueueStreamer {

	CONSUMER_INTERFACE(Consumer,Frame)
	EVENT_INTERFACE(HasData)

	static Handle<Frame> h;
	static uint8_t index;

	struct Stream {
		static inline bool fetch(uint8_t *container) {
			if (!h.isValid()) {
				h = Consumer::dequeue();
				if (!h.isValid()) return false;

				index = 0;
			}

			*container = h.buffer->data[index];
			++index;
			if (index == h.buffer->length) {
				Consumer::recycle(h);
				h.destroy();
			}

			return true;
		}
	};
	
	struct DataAvailable {
		static inline void signal() {
			if (!h.isValid()) {
				h = Consumer::dequeue();
				if (!h.isValid()) return;

				index = 0;
				HasData::signal();
			}
		}
	};
};

template<typename Owner>
Handle<Frame> QueueStreamer<Owner>::h;
template<typename Owner>
uint8_t QueueStreamer<Owner>::index;


BIND_STREAM(QueueStreamer<>::Stream, Printer<>::Stream, uint8_t)
BIND_EVENT(Printer<>::DataAvailable, QueueStreamer<>::HasData)

typedef BufferQueue<Frame, 10> BufferQueue_of_QueueStreamer;
BIND_CONSUMER(BufferQueue_of_QueueStreamer::Consumer, QueueStreamer<>::Consumer, Frame)
BIND_EVENT(QueueStreamer<>::DataAvailable, BufferQueue_of_QueueStreamer::FirstItem)

BIND_PRODUCER(BufferQueue_of_QueueStreamer::Producer, FrameAssembler<MainModule>::Producer, Frame)

BIND_STREAM(Source<>::Stream, FrameAssembler<MainModule>::Stream, uint8_t)
BIND_EVENT(FrameAssembler<MainModule>::DataAvailable, Source<>::HasData)

void frameassembler() {
	std::cout << "----- Frame assembler -----" << std::endl;

	const char frame[] = "~\xFE\xFE\x01\x40\x00\x41";

	for (int i = 0; i < sizeof(frame); ++i) {
		Source<>::write(frame[i]);
	}

	//Printer<>::trigger();
}


int main()
{
	int N = 20;

	for (int i = 0; i < N; i++) framestreamer();
	getchar();

	for (int i = 0; i < N; i++) frameassembler();
	getchar();
	
    return 0;
}
