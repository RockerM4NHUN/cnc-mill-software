#pragma once

#include <array>

#include "es_base.h"
#include "queue.h"


template<typename T, int N, typename Owner = MainModule>
class BufferQueue {
public:
	EVENT_INTERFACE(FirstItem)
private:

	static std::array<T, N> buffers;
	static Queue<uint8_t, N> enqueued;
	static Queue<uint8_t, N> recycled;

public:


	class Producer {
	public:
		// serves an empty buffer for use
		static Handle<T> requestBuffer() {
			Handle<T> h;
			
			if (recycled.consume(&h.id)) {
				h.buffer = &buffers[h.id];
			}

			return h;
		}

		// puts buffer in output queue
		static void enqueue(Handle<T> h) {
			enqueued.deposit(h.id);
		}
	};

	class Consumer {
	public:

		static Handle<T> dequeue() {
			Handle<T> h;

			if (enqueued.consume(&h.id)) {
				h.buffer = &buffers[h.id];
			}

			return h;
		}

		static void recycle(Handle<T> h) {
			recycled.deposit(h.id);
		}
	};
};

template<typename T, int N, typename Owner> std::array<T, N> BufferQueue<T, N, Owner>::buffers;
template<typename T, int N, typename Owner> Queue<uint8_t, N> BufferQueue<T, N, Owner>::enqueued(FirstItem::signal);
template<typename T, int N, typename Owner> Queue<uint8_t, N> BufferQueue<T, N, Owner>::recycled =
[]() {
	Queue<uint8_t, N> q;
	for (uint8_t i = 0; i < N; ++i) {
		q.deposit(i);
	}
	return q;
}();
