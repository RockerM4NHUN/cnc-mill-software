#pragma once

struct MainModule{};

template<typename T>
struct Handle {
	T* buffer;
	uint8_t id;

	Handle() : buffer(0) {}

	bool isValid() {
		return buffer != nullptr;
	}

	void destroy() {
		buffer = 0;
	}
};


// Consumer interface

#define CONSUMER_INTERFACE(name,type)			\
struct name {									\
	static inline Handle<type> dequeue();		\
	static inline void recycle(Handle<type> h);	\
};

#define BIND_CONSUMER(consumer, iface, type)			\
template<> inline Handle<type> iface::dequeue() {		\
	return consumer::dequeue();							\
}														\
template<> inline void iface::recycle(Handle<type> h) {	\
	consumer::recycle(h);								\
}


// Producer interface

#define PRODUCER_INTERFACE(name,type)			\
struct name {									\
	static inline Handle<type> requestBuffer();	\
	static inline void enqueue(Handle<type> h);	\
};

#define BIND_PRODUCER(producer, iface, type)			\
template<> inline Handle<type> iface::requestBuffer() {	\
	return producer::requestBuffer();					\
}														\
template<> inline void iface::enqueue(Handle<type> h) {	\
	producer::enqueue(h);								\
}


// Stream interface

#define STREAM_INTERFACE(name,type)				\
struct name {									\
	static inline bool fetch(type* container);	\
};

#define BIND_STREAM(stream, iface, type)				\
template<> inline bool iface::fetch(type* container) {	\
	return stream::fetch(container);					\
}


// Event interface

#define EVENT_INTERFACE(name)		\
struct name {						\
	static inline void signal();	\
};

#define BIND_EVENT(event, iface)		\
template<> inline void iface::signal(){	\
	event::signal();					\
}

#define __noop noop
void noop() {}
