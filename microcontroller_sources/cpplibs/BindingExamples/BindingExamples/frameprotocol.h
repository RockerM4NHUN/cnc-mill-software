#pragma once

#include <cstdint>
#include <array>

#include "es_base.h"

// special characters in byte stream, should never be less than 0x40 and more than 0x7F
const uint8_t ESCAPE = 0x5C;// backslash
const uint8_t SOF = 0x7E;	// ~
const uint8_t ACK = 0x7D;	// }
const uint8_t NAK = 0x7B;	// {
const uint8_t RER = 0x7C;	// |

inline bool isControlByte(uint8_t data) {
	return data > 0x7A && data < 0x7F;
}

inline bool isEscaped(uint8_t c) {
	return c == 0x5C || ((uint8_t)(c - 0x7B)) < 4;
}

// escape and deescape characters
#define DO_ESCAPE(C) (C - (C >> 4) - 18)
#define DE_ESCAPE(C) (C + (C >> 4) + 19)

const uint8_t MAX_ID = 64;
const uint8_t MAX_FRAME_LENGTH = 127;

struct Frame {
	std::array<uint8_t, MAX_FRAME_LENGTH> data;
	uint16_t checksum;
	uint8_t length;
	uint8_t frameID;

	Frame() : checksum(0), length(0), frameID(0) {}

	void addByte(uint8_t b) {
		if (length == MAX_FRAME_LENGTH) return;
		data[length] = b;
		++length;
	}

	void reset() {
		length = 0;
		frameID = 0;
	}
};
