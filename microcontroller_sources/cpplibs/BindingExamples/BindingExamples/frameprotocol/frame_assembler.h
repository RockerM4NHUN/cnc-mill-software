#pragma once

#include "../bufferqueue.h"
#include "../frameprotocol.h"

template<typename Owner>
class FrameAssembler {

	STREAM_INTERFACE(Stream,uint8_t)
	PRODUCER_INTERFACE(Producer,Frame)
	EVENT_INTERFACE(NAKEvent)
	EVENT_INTERFACE(ACKEvent)
	EVENT_INTERFACE(ResetEvent)
	EVENT_INTERFACE(CorruptionEvent)

	static Handle<Frame> handle;

	static bool escaped;
	static uint8_t frameState;
	static uint8_t dataIndex;

	static void reset() {
		escaped = false;
		frameState = 0;
		dataIndex = 0;
	}

public:

	// called by byte receiver to indicate available data
	struct DataAvailable {
		static void signal() {
			uint8_t data;
			uint8_t length2;

			if (!handle.isValid()) {
				handle = Producer::requestBuffer();
				if (handle.isValid()) {
					reset();
					handle.buffer->reset();
				} else {
					return;
				}
			}

			if (Stream::fetch(&data)) {

				if (isControlByte(data)) {
					// control byte received
					reset();
					handle.buffer->reset();
				}

				switch (frameState) {
				case 0:
					switch (data) {
					case RER:
						// TODO propagate communication reset request
						reset();
						break;
					case ACK:
						// TODO flag ACK
						break;
					case NAK:
						// TODO flag NAK
						break;
					case SOF:
						++frameState;
						break;
					}
					break;
				case 1:
					handle.buffer->length = (uint8_t)((~data) & 0xFF);
					++frameState;
					break;
				case 2:
					length2 = (uint8_t)((~data) & 0xFF);
					if (handle.buffer->length == length2) {
						++frameState;
					} else {
						reset();
						// TODO flag message inconsistency
					}
					break;
				case 3:
					if (data == 0 || data > MAX_ID) {
						// invalid frame ID
						reset();
						// TODO flag message inconsistency
					} else {
						handle.buffer->frameID = data;
						++frameState;
					}
					break;
				case 4:
					if (dataIndex < handle.buffer->length) { // payload
						if (data == ESCAPE) {
							escaped = true;
						} else {
							if (escaped) {
								data = DE_ESCAPE(data);
							}

							handle.buffer->data[dataIndex] = data;
							++dataIndex;
						}
					} else {
						++frameState;
					}
					break;
				case 5:
					handle.buffer->checksum = data << 8;
					++frameState;
					break;
				case 6:
					handle.buffer->checksum += data;

					// full frame received, send to protocol
					Producer::enqueue(handle);
					handle.destroy();
					break;
				}
			}
		}
	};

};

template<typename Owner>
Handle<Frame> FrameAssembler<Owner>::handle;
template<typename Owner>
bool FrameAssembler<Owner>::escaped;
template<typename Owner>
uint8_t FrameAssembler<Owner>::frameState;
template<typename Owner>
uint8_t FrameAssembler<Owner>::dataIndex;