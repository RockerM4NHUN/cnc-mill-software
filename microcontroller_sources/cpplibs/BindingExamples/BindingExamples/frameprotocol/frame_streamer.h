#pragma once

#include "../frameprotocol.h"

template<typename Owner>
class FrameStreamer {
public:
	CONSUMER_INTERFACE(Consumer, Frame)
	EVENT_INTERFACE(HasData)

private:
	static Handle<Frame> handle;

	static uint8_t frameState;
	static uint8_t dataIndex;
	static bool escaped;

	static bool awaitAck;
	static bool transmitAck;
	static bool transmitNak;
	static bool streamBusy;

	static void sendCurrentFrame() {

		frameState = 0;
		dataIndex = 0;
		escaped = false;

		awaitAck = false; // must be last assign

		if (!streamBusy && handle.isValid()) {
			streamBusy = true;
			HasData::signal();
		}
	}

public:

	struct FrameAvailableEvent {
		static inline void signal() {
			// is frame already streamed?
			if (handle.isValid()) return;

			// is there new frame to be sent?
			handle = Consumer::dequeue();
			if (!handle.isValid()) return;

			if (!streamBusy) sendCurrentFrame();
		}
	};

	struct ACKTimeoutEvent {
		static void signal() {
			sendCurrentFrame();
		}
	};

	struct NAKEvent {
		static void signal() {
			sendCurrentFrame();
		}
	};

	struct ACKEvent {
		static void signal() {
			if (awaitAck) {
				awaitAck = false;
				// the full frame is sent and confirmed, return buffer
				Consumer::recycle(handle);
				handle.destroy();
				FrameAvailableEvent::signal();
			} else {
				// received ACK when frame is not sent yet
				sendCurrentFrame();
			}
		}
	};

	struct CorruptionEvent {
		static void signal() {
			transmitNak = true;
			if (!streamBusy) {
				streamBusy = true;
				HasData::signal();
			}
		}
	};

	struct SuccessEvent {
		static void signal() {
			transmitAck = true;
			if (!streamBusy) {
				streamBusy = true;
				HasData::signal();
			}
		}
	};

	// called by the byte output mechanism for new byte to send
	struct Stream {
		static inline bool fetch(uint8_t* container) {
			uint8_t data;

			// if frame is not in transmission
			if (frameState == 0) {
				if (transmitAck) {
					transmitAck = false;
					*container = ACK;
					return true;
				}
				
				if (transmitNak) {
					transmitNak = false;
					*container = NAK;
					return true;
				}
			}

			if (awaitAck) {
				streamBusy = false;
				return false;
			}

			// currently no data available, try to fetch
			if (!handle.isValid()) {
				handle = Consumer::dequeue();
				if (!handle.isValid()) {
					streamBusy = false;
					return false;
				}

				// prepare for sending new frame
				sendCurrentFrame();
			}

			if (escaped) {
				escaped = false;
				*container = DO_ESCAPE(handle.buffer->data[dataIndex]);
				++dataIndex;

				return true;
			}

			switch (frameState) {
				case 0:
					*container = SOF;
					++frameState;
					break;
				case 1:
				case 2:
					*container = (uint8_t)((~handle.buffer->length) & 0xFF);
					++frameState;
					break;
				case 3:
					*container = handle.buffer->frameID;
					++frameState;
					break;
				case 4:

					if (dataIndex < handle.buffer->length) {
						data = handle.buffer->data[dataIndex];
						if (isEscaped(data)) {
							escaped = true;
							*container = '\\';
						} else {
							*container = data;
							++dataIndex;
						}
					} else {
						*container = (uint8_t)((handle.buffer->checksum >> 8) & 0xFF);
						++frameState;
					}
					break;
				case 5:
					awaitAck = true; // order of assign is important!
					frameState = 0;
					*container = (uint8_t)((handle.buffer->checksum >> 0) & 0xFF);
					break;
			}

			return true;
		}
	};

	
	
};

template<typename Owner>
Handle<Frame> FrameStreamer<Owner>::handle;
template<typename Owner>
uint8_t FrameStreamer<Owner>::frameState;
template<typename Owner>
uint8_t FrameStreamer<Owner>::dataIndex;
template<typename Owner>
bool FrameStreamer<Owner>::escaped;
template<typename Owner>
bool FrameStreamer<Owner>::awaitAck;
template<typename Owner>
bool FrameStreamer<Owner>::transmitAck;
template<typename Owner>
bool FrameStreamer<Owner>::transmitNak;
template<typename Owner>
bool FrameStreamer<Owner>::streamBusy;