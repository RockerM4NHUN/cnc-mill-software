// BindingExamples.cpp : Defines the entry point for the console application.
//

#include <array>

template<typename T, void(*Event)(T)>
struct IRead {
	static void read(); // command
	static constexpr void(*readDone)(T) = Event;
};

class Accelerometer {
	Accelerometer() = delete;

	static int resource;

public:
	template<void(*Event)(int)>
	class XComponent : public IRead<int, Event> {
	public:
		static void read() {
			XComponent::readDone(13);
		}
	};
};




int result = 1;

void handleReadResult(int value) {
	result = value;
}

int main()
{
	Accelerometer::XComponent<handleReadResult>::read();

    return result;
}

