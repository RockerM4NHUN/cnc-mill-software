#pragma once

#include <array>

template<uint8_t N>
struct IndexSet {
	uint8_t head;
	uint8_t tail;
	std::array<uint8_t, N> linking;

	static const uint8_t EMPTY = 255;

	IndexSet() : IndexSet(false) {}

	/*IndexSet(bool full) : head(full ? 0 : EMPTY), tail(full ? N-1 : EMPTY){
	// TODO initialize linking array with template magic
	}*/

	IndexSet(bool full) : head(EMPTY), tail(EMPTY) {
		if (full) {
			for (uint8_t i = 0; i < N; ++i) {
				linking[i] = i + 1;
			}
			linking[N - 1] = 0;
		} else {
			for (uint8_t i = 0; i < N; ++i) {
				linking[i] = EMPTY;
			}
		}
	}

	void push(uint8_t id) {
		if (linking[id] == EMPTY && tail != id) {
			if (head == EMPTY) {
				head = id;
				tail = id;
			} else {
				linking[tail] = id;
				tail = id;
			}
		}
	}

	uint8_t pop() {
		if (head != EMPTY) {

			uint8_t id = head;
			head = linking[head];

			if (head == EMPTY) {
				tail = EMPTY;
			}

			linking[id] = EMPTY;

			return id;
		}
		return EMPTY;
	}

	bool isEmpty() {
		return head == EMPTY;
	}
};
