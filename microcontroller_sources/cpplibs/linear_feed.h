#pragma once

#include <cstdint>

#include "output_queue.h"


class LinearFeedModule {

	typedef struct Segment {
		int8_t dx;
		int8_t dy;
		int8_t dz;
		uint16_t periodx;
		uint16_t periody;
		uint16_t periodz;
	};

	void dispatchSegment(Segment* segment);	// ensures the execution of the segment
	void stopSegmentFeed();					// stops the execution of the current segment and halts the segment feed

	void triggerLinearMovement();

	static OutputQueue<Segment, triggerLinearMovement, 16> linearFeed;

	// Soft Implementation

	static Segment currentSegment;
	void triggerLinearMovement() {
		if (linearFeed.consume(&currentSegment)) {
			outputSegment(currentSegment);
		} else {
			stopSegmentFeed();
		}
	}

};