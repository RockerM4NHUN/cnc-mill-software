#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h> // needed for memset

int main()
{
	struct termios tio, oldtio;
	struct termios stdio, oldstdio;
	int tty_fd;
	
	const char *uart_device = "/dev/ttyS1";
	
	printf("UART opened successfully\n");
	
	memset(&oldstdio,0,sizeof(oldstdio));
	memset(&oldtio,0,sizeof(oldtio));
	
	tcgetattr(STDOUT_FILENO,&oldstdio);

	memset(&stdio,0,sizeof(stdio));
	stdio.c_iflag=0;
	stdio.c_oflag=0;
	stdio.c_cflag=0;
	stdio.c_lflag=0;
	stdio.c_cc[VMIN]=1;
	stdio.c_cc[VTIME]=0;
	tcsetattr(STDOUT_FILENO,TCSANOW,&stdio);
	tcsetattr(STDOUT_FILENO,TCSAFLUSH,&stdio);
	fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);       // make the reads non-blocking

	memset(&tio,0,sizeof(tio));
	tio.c_iflag=0;
	tio.c_oflag=0;
	tio.c_cflag=CS8|CREAD|CLOCAL;           // 8n1, see termios.h for more information
	tio.c_lflag=0;
	tio.c_cc[VMIN]=1;
	tio.c_cc[VTIME]=5;

	tty_fd=open(uart_device, O_RDWR | O_NONBLOCK);        // O_NONBLOCK might override VMIN and VTIME, so read() may return immediately.
	
	tcgetattr(tty_fd,&oldtio);
	
	cfsetospeed(&tio,B115200);            // 115200 baud
	cfsetispeed(&tio,B115200);            // 115200 baud

	tcsetattr(tty_fd,TCSANOW,&tio);
	
	unsigned char c='D';
	
	while (c!='q'){
		
		if (read(tty_fd,&c,1)>0){
			write(STDOUT_FILENO,&c,1);	// if new data is available on the serial port, print it out
		}
		
		if (read(STDIN_FILENO,&c,1)>0){
			write(tty_fd,&c,1);			// if new data is available on the console, send it to the serial port
		}
	}
	
	
	tcsetattr(STDOUT_FILENO,TCSANOW,&oldstdio);
	tcsetattr(STDOUT_FILENO,TCSAFLUSH,&oldstdio);
	
	tcsetattr(tty_fd,TCSANOW,&oldtio);
	tcsetattr(tty_fd,TCSAFLUSH,&oldtio);

	close(tty_fd);
	return 0;
}