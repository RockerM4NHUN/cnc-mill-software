/*
 * stepping.cpp
 *
 *  Created on: 2018. jan. 9.
 *      Author: Projekt
 */

#include <math.h>
#include <stdint.h>
#include <array>

#include "app/stepping.h"
#include "app/motor_bindings.h"

using namespace std;

// 72MHz clk and described PWM -> more than 8500 maximum steps per second without microstep loss

// PWM is 7 bit in value, 64 steps in time
const uint8_t MICROSTEPS = 16;

// 7 bit in value, 0 to PI/2 in 64 steps
const int PWM_RESOLUTION = 128;

const array<int16_t, MICROSTEPS> sineZeroToHalfPi = {0, 12, 24, 37, 48, 60, 71, 81, 90, 98, 106, 112, 118, 122, 125, 127};

// 64 microsteps
//const array<int16_t, MICROSTEPS> sineZeroToHalfPi = {0, 3, 6, 9, 12, 15, 18, 21, 24, 28, 31, 34, 37, 40, 43, 46, 48, 51, 54, 57, 60, 63, 65, 68, 71, 73, 76, 78, 81, 83, 85, 88, 90, 92, 94, 96, 98, 100, 102, 104, 106, 108, 109, 111, 112, 114, 115, 117, 118, 119, 120, 121, 122, 123, 124, 124, 125, 126, 126, 127, 127, 127, 127, 127};
//const array<int16_t, MICROSTEPS> sineZeroToHalfPi = {0, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127};



float dticks;
float errorticks;
uint8_t microstepCounter;	// current microstep

// mapping for sine wave
#define _0toP (sineZeroToHalfPi[microstepCounter])
#define _0toM (-sineZeroToHalfPi[microstepCounter])
#define _Pto0 (sineZeroToHalfPi[MICROSTEPS - microstepCounter - 1])
#define _Mto0 (-sineZeroToHalfPi[MICROSTEPS - microstepCounter - 1])


struct BipolarMotor{
	BipolarMotor(void(*actuate)(int16_t coilA, int16_t coilB)) : machinePosition(0), isBackward(0), phase(0), moving(false), actuate(actuate) {}

	int32_t machinePosition;	// step count from machine origin
	int8_t isBackward;			// bipolar stepper direction
	uint8_t phase;				// bipolar stepper phase
	bool moving;

	void(*actuate)(int16_t coilA, int16_t coilB);

	void doMicrostep(){
		int16_t coilA, coilB;

		if(moving){
			if (!isBackward){
				switch(phase){
				case 1: // Coil A 0 to +, coil B - to 0
					coilA = _0toP;
					coilB = _Mto0;
					break;
				case 2: // Coil A + to 0, coil B 0 to +
					coilA = _Pto0;
					coilB = _0toP;
					break;
				case 3: // Coil A 0 to -, coil B + to 0
					coilA = _0toM;
					coilB = _Pto0;
					break;
				case 0: // Coil A - to 0, coil B 0 to -
					coilA = _Mto0;
					coilB = _0toM;
					break;
				}

			} else {
				switch(phase){
				case 0: // Coil A + to 0, coil B 0 to -
					coilA = _Pto0;
					coilB = _0toM;
					break;
				case 1: // Coil A 0 to -, coil B - to 0
					coilA = _0toP;
					coilB = _Pto0;
					break;
				case 2: // Coil A - to 0, coil B 0 to +
					coilA = _Mto0;
					coilB = _0toP;
					break;
				case 3: // Coil A 0 to +, coil B - to 0
					coilA = _0toM;
					coilB = _Mto0;
					break;
				}
			}

			actuate(coilA, coilB);
		}
	}
};

array<BipolarMotor, 3> motors = {
	BipolarMotor(actuateX),
	BipolarMotor(actuateY),
	BipolarMotor(actuateZ)
};




// Helper for squaring
template<class T>
constexpr inline T sqr(T o1) {
	return o1*o1;
}






void step(array<BipolarMotor*, 3> motors, array<int8_t, 3> directions, uint16_t ticks){

	dticks = ticks/(float)MICROSTEPS;

	if (dticks < 50){
		errorticks = 1;
		errorticks = 1;
		errorticks = 1;
	}

	errorticks = 0;

	for(uint8_t i = 0; i < motors.size(); i++){
		if (directions[i] != 0){
			motors[i]->machinePosition += directions[i];

			motors[i]->isBackward = (directions[i] < 0);
			motors[i]->phase += directions[i];
			motors[i]->phase &= 0x03;
			motors[i]->moving = true;
		} else {
			motors[i]->moving = false;
		}
	}

}









struct Segment;

bool doLinearStep(Segment* s);
bool doSlowArcStep(Segment* s);
bool doFastArcStep(Segment* s);

struct Segment {

	enum Type {
		LINEAR,					// straight path
		SLOW_ARC,				// arc path, where circular motion is faster than translational
		FAST_ARC				// arc path, where circular motion is slower than translational
	};

	typedef bool(*StepFunction)(Segment*);



	/*********************** LINEAR ***********************/

	static Segment* linear(array<BipolarMotor*, 3> motors, array<int32_t, 3> distance, float speed, float acceleration) {
		Segment* s = new Segment(LINEAR, doLinearStep, motors, distance, speed, acceleration);

		s->initializeLinear();

		return s;
	}

	Segment(Type type, StepFunction doStep, array<BipolarMotor*, 3> motors, array<int32_t, 3> distance, float speed, float acceleration)
		: type(type), doStep(doStep), motors(motors), distance(distance), speed(speed), acceleration(acceleration) {}

	void inline initializeLinear() {

		// transform distance vector to the positive quadrant
		for (uint8_t i = 0; i < distance.size(); i++) {
			direction[i] = distance[i] > 0 ? 1 : (distance[i] < 0 ? -1 : 0);
			distance[i] = distance[i] < 0 ? -distance[i] : distance[i];
		}

		// select dominant axis
		if (distance[X] < distance[Y] || distance[X] < distance[Z]) {
			// x is not dominant, swap needed
			if (distance[Y] > distance[Z]) {
				// y is dominant
				swap(motors[X], motors[Y]);
				swap(distance[X], distance[Y]);
				swap(direction[X], direction[Y]);
			}
			else {
				// z is dominant
				swap(motors[X], motors[Z]);
				swap(distance[X], distance[Z]);
				swap(direction[X], direction[Z]);
			}
		}

		// calculate slope for secondary axes
		dy = abs(((float)(distance[Y])) / ((float)(distance[X])));
		dz = abs(((float)(distance[Z])) / ((float)(distance[X])));

		// initialize errors (starting point is on raster hence initial errors must be zero)
		errory = 0;
		errorz = 0;
	}



	/*********************** ARC ***********************/

	/*
		Arc segment with all the angles to the contained points in the interval of [k*45� ; (k+1)*45�], k is integer.
		Arc is defined by three points and (redundantly) the radius. X and Y will produce the arc, Z has linear movement.
		The axes should define a right handed coordinate system in reality.

		start: relative position from the origin, on the raster
		distance: vector from start to end, both on raster
	 */
	static Segment* arc(array<BipolarMotor*, 3> motors, array<int32_t, 3> distance, float speed, float acceleration, array<int32_t, 2> start, float radius) {

		if (abs(distance[Z]) > abs(distance[X]) && abs(distance[Z]) > abs(distance[Y])) {
			Segment* s = new Segment(FAST_ARC, doFastArcStep, motors, distance, speed, acceleration, start, radius);
			s->initializeFastArc();
			return s;
		} else {
			Segment* s = new Segment(SLOW_ARC, doSlowArcStep, motors, distance, speed, acceleration, start, radius);
			s->initializeSlowArc();
			return s;
		}
	}

	Segment(Type type, StepFunction doStep, array<BipolarMotor*, 3> motors, array<int32_t, 3> distance, float speed, float acceleration, array<int32_t, 2> start, float radius)
		: type(type), doStep(doStep), motors(motors), distance(distance), speed(speed), acceleration(acceleration), start(start), radiusSqr(radius*radius) {}

	void inline initializeSlowArc() {

		// transform distance vector to the positive quadrant
		for (uint8_t i = 0; i < distance.size(); i++) {
			direction[i] = distance[i] > 0 ? 1 : (distance[i] < 0 ? -1 : 0);
			distance[i] = distance[i] < 0 ? -distance[i] : distance[i];
		}

		// select dominant axis
		if (distance[Y] > distance[X]) {
			// y is dominant
			swap(motors[X], motors[Y]);
			swap(distance[X], distance[Y]);
			swap(direction[X], direction[Y]);
			swap(start[X],start[Y]);
		}

		// calculate slope for linear axis
		dz = abs(((float)(distance[Z])) / ((float)(distance[X])));

		// initialize linear error (starting point is on raster hence initial error must be zero)
		errorz = 0;
	}

	void inline initializeFastArc() {

		// transform distance vector to the positive quadrant
		for (uint8_t i = 0; i < distance.size(); i++) {
			direction[i] = distance[i] > 0 ? 1 : (distance[i] < 0 ? -1 : 0);
			distance[i] = distance[i] < 0 ? -distance[i] : distance[i];
		}

		// select dominant axis
		if (distance[Y] > distance[X]) {
			// y is dominant
			swap(motors[X], motors[Y]);
			swap(distance[X], distance[Y]);
			swap(direction[X], direction[Y]);
			swap(start[X], start[Y]);
		}

		// calculate slope for linear axis
		dx = abs(((float)(distance[X])) / ((float)(distance[Z])));

		// initialize linear error (starting point is on raster hence initial error must be zero)
		errorx = 0;
	}






	/*********************** Data ***********************/

	Type type;
	StepFunction doStep;		// initialize step sequence, returns true if segment cannot make more steps (finished)

	array<BipolarMotor*, 3> motors;	// hold motor handles, it will be rearranged for rasterizer simplicity
	array<int32_t, 3> distance;	// distance in steps from the target location
	array<int8_t, 3> direction;	// direction of section on axis (+1/0/-1)

	float speed;				// speed in eucledian space, step/tick
	float acceleration; 		// acceleration in eucledian space, step/tick^2

	// Data for LINEAR
	float dy;
	float dz;
	float errory;
	float errorz;

	// Data for ARC
	array<int32_t, 2> start;
	float radiusSqr;
	float dx;
	float errorx;
};














bool doLinearStep(Segment* s) {

	array<int8_t, 3> d;

	s->errory += s->dy;
	s->errorz += s->dz;

	s->distance[X]--;

	int numOfSteppedDirections = 1;
	d[X] = s->direction[X];
	d[Y] = 0;
	d[Z] = 0;

	if (s->errory >= 0.5) {
		numOfSteppedDirections++;
		s->distance[Y]--;
		s->errory -= 1;
		d[Y] = s->direction[Y];
	}

	if (s->errorz >= 0.5) {
		numOfSteppedDirections++;
		s->distance[Z]--;
		s->errorz -= 1;
		d[Z] = s->direction[Z];
	}

	static const float timeFactor[3]{ 1, sqrt(2), sqrt(3) };

	float time = timeFactor[numOfSteppedDirections - 1] / s->speed;
	s->speed += s->acceleration;

	uint16_t ticks = time;

	// Actuate motors
	step(s->motors, d, ticks);

	// return if no steps left in segment
	return s->distance[X] == 0;
}






bool doSlowArcStep(Segment* s) {

	array<int8_t, 3> d;


	// arc movement
	float errorx = abs(sqr(s->start[X] + s->direction[X]) + sqr(s->start[Y]) - s->radiusSqr);
	float errorxy = abs(sqr(s->start[X] + s->direction[X]) + sqr(s->start[Y] + s->direction[Y]) - s->radiusSqr);

	int numOfSteppedDirections = 1;

	s->start[X] += s->direction[X];
	d[X] = s->direction[X];
	s->distance[X]--;

	d[Y] = 0;

	if (errorxy < errorx) {
		// step on Y axis too
		numOfSteppedDirections++;
		s->start[Y] += s->direction[Y];
		s->distance[Y]--;
		d[Y] = s->direction[Y];
	}

	s->errorz += s->dz;
	d[Z] = 0;

	// linear movement
	if (s->errorz >= 0.5) {
		numOfSteppedDirections++;
		s->distance[Z]--;
		s->errorz -= 1;
		d[Z] = s->direction[Z];
	}

	static const float timeFactor[3]{ 1, sqrt(2), sqrt(3) };

	float time = timeFactor[numOfSteppedDirections - 1] / s->speed;
	s->speed += s->acceleration;

	uint16_t ticks = time;

	// Actuate motors
	step(s->motors, d, ticks);

	// return if no steps left in segment
	return s->distance[X] == 0;
}






bool doFastArcStep(Segment* s) {

	array<int8_t, 3> d;

	d[X] = 0;
	d[Y] = 0;

	int numOfSteppedDirections = 1;
	d[Z] = s->direction[Z];
	s->distance[Z]--;

	s->errorx += s->dx;

	// linear movement
	if (s->errorx >= 0.5) {
		numOfSteppedDirections++;
		s->distance[X]--;
		s->errorx -= 1;
		d[X] = s->direction[X];
		s->start[X] += s->direction[X];

		// arc movement
		float errorx = abs(sqr(s->start[X]) + sqr(s->start[Y]) - s->radiusSqr);
		float errorxy = abs(sqr(s->start[X]) + sqr(s->start[Y] + s->direction[Y]) - s->radiusSqr);

		if (errorxy < errorx) {
			// step on Y axis too
			numOfSteppedDirections++;
			s->start[Y] += s->direction[Y];
			s->distance[Y]--;
			d[Y] = s->direction[Y];
		}
	}

	static const float timeFactor[3]{ 1, sqrt(2), sqrt(3) };

	float time = timeFactor[numOfSteppedDirections - 1] / s->speed;
	s->speed += s->acceleration;

	uint16_t ticks = time;

	// Actuate motors
	step(s->motors, d, ticks);

	// return if no steps left in segment
	return s->distance[Z] == 0;
}











array<Segment*, 20> segments;
uint8_t segmentindex = 0;
uint8_t segmentcount = 0;
Segment* currentSegment = nullptr;

float speed = 1.0f/50000;	// step/tick (speed * 72000000 step/second), speed > 1/65535

void testGenerateCircle(){
	array<BipolarMotor*, 3> hmotors = {&motors[X],&motors[Y],&motors[Z]};

	int32_t radius = 400;

	array<int32_t, 2> current = {0, -radius};
	array<int32_t, 2> next = {0, 0};

	for(float angle = -M_PI_4; angle < (2*M_3PI_4 + 1e-3f); angle += M_PI_4){

		next[X] = radius*cos(angle);
		next[Y] = radius*sin(angle);

		segments[segmentcount++] = (Segment::arc(hmotors, array<int32_t, 3>{next[X] - current[X], next[Y] - current[Y], 0}, speed, 0, current, radius));

		current[X] = next[X];
		current[Y] = next[Y];
	}
}

void testGenerateLinear(){
	array<BipolarMotor*, 3> hmotors = {&motors[X],&motors[Y],&motors[Z]};

	for(float speed = 1.0/50000; speed < 3.0/50000; speed *= 2){
		segments[segmentcount++] = (Segment::linear(hmotors, array<int32_t, 3>{100, 0, 0}, speed, 0));
	}

	//segments[segmentcount++] = (Segment::linear(hmotors, array<int32_t, 3>{400, 200, 0}, speed, 0));
}

void testGenerateLinearAcceleration(){
	array<BipolarMotor*, 3> hmotors = {&motors[X],&motors[Y],&motors[Z]};

	segments[segmentcount++] = (Segment::linear(hmotors, array<int32_t, 3>{400, 200, 0}, speed, 0));
	segments[segmentcount++] = (Segment::linear(hmotors, array<int32_t, 3>{400, 200, 0}, speed, speed/400));
	segments[segmentcount++] = (Segment::linear(hmotors, array<int32_t, 3>{400, 200, 0}, 2*speed, 0));
	segments[segmentcount++] = (Segment::linear(hmotors, array<int32_t, 3>{400, 200, 0}, 2*speed, 0));
	segments[segmentcount++] = (Segment::linear(hmotors, array<int32_t, 3>{400, 200, 0}, 2*speed, -speed/400));
	segments[segmentcount++] = (Segment::linear(hmotors, array<int32_t, 3>{-400, -200, 0}, speed, 0));
	segments[segmentcount++] = (Segment::linear(hmotors, array<int32_t, 3>{-400, -200, 0}, speed, speed/400));
	segments[segmentcount++] = (Segment::linear(hmotors, array<int32_t, 3>{-400, -200, 0}, 2*speed, 0));
	segments[segmentcount++] = (Segment::linear(hmotors, array<int32_t, 3>{-400, -200, 0}, 2*speed, 0));
	segments[segmentcount++] = (Segment::linear(hmotors, array<int32_t, 3>{-400, -200, 0}, 2*speed, -speed/400));
}

#include <stm32f3xx_hal.h>

extern "C" void microstepInterrupt(){

	HAL_GPIO_WritePin(LD2_GPIO_Port,LD2_Pin,GPIO_PIN_RESET);

	static bool segchange = false;

	if (segmentindex == segmentcount){
		segmentindex = 0;
		segmentcount = 0;
		//testGenerateLinear();
		//testGenerateCircle();
		testGenerateLinearAcceleration();
	}

	if (currentSegment == nullptr){
		if (segmentindex < segmentcount){ // if array contains segments
			currentSegment = segments[segmentindex];
			segmentindex++;
			segchange = true;
		} else {
			return;
		}
	}

	if (microstepCounter == 0){

		//HAL_GPIO_TogglePin(LD2_GPIO_Port,LD2_Pin);

		if (currentSegment->doStep(currentSegment)){
			// no steps left in segment, on next step fetch new one
			delete currentSegment;
			currentSegment = nullptr;
		}

		if (segchange){
			segchange = false;
			segchange = false;
		}
	}

	for(uint8_t i = 0; i < motors.size(); i++){
		motors[i].doMicrostep();
	}

	// set interval until next interrupt
	errorticks += dticks;
	uint16_t ticks = round(errorticks);
	errorticks -= ticks;
	setMicrostepLength(ticks);

	microstepCounter++;
	if (microstepCounter >= MICROSTEPS){
		microstepCounter = 0;
	}

	HAL_GPIO_WritePin(LD2_GPIO_Port,LD2_Pin,GPIO_PIN_SET);
}

