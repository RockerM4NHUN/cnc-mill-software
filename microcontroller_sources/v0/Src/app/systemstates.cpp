/*
 * systemstates.cpp
 *
 *  Created on: 2017. dec. 29.
 *      Author: Projekt
 */

#include <app/systemstates.h>

StateMachine stateMachine;

/* Generated content */

StateMachine::State settingsState(SettingsAction);
StateMachine::State idleState(IdleAction);
StateMachine::State executionState(ExecutionAction);

/* Generated content end */

StateMachine generateStateMachine(){

	StateMachine stateMachine;

	/* Generated content */

	settingsState.addTransition(&idleState, SettingsToIdleTransition);

	idleState.addTransition(&settingsState, IdleToSettingsTransition);
	idleState.addTransition(&executionState, IdleToExecutionTransition);

	executionState.addTransition(&idleState, ExecutionToIdleTransition);

	stateMachine.setInitialState(&settingsState);

	/* Generated content */

	return stateMachine;
}
