/*
 * frameprotocol.cpp
 *
 *  Created on: 2017. dec. 9.
 *      Author: Projekt
 */

#include <app/frameprotocol.h>
#include <cstring>

// special characters in byte stream, should never be less than 0x41 and more than 0x7F
/*
#define ESCAPE 0x5C // backslash
#define SOF 0x7E	// ~
#define ACK 0x7D	// }
#define NAK 0x7B	// {
#define SOC 0x7C	// |

#define DO_ESCAPE(C) ((C) - ((C) >> 4) - 18)
#define DE_ESCAPE(C) ((C) + ((C) >> 4) + 19)
#define IS_ESCABABLE(C) ((C) == ESCAPE || !((C) < NAK || (C) > SOF))
*/

#define ESCAPE	0x5C	// backslash
#define SOF		0x5E	// ^
#define ACK		0x5D	// ]
#define NAK		0x5B	// [

#define DO_ESCAPE(C) ((C) - 0x19)
#define DE_ESCAPE(C) ((C) + 0x19)
#define IS_ESCABABLE(C) ((C) > 0x5A && (C) < 0x5F)

#define MAX_ID 64

#define MAX_FRAME_LENGTH 127
#define BUFFER_LENGTH (MAX_FRAME_LENGTH+10)

EndPoint::EndPoint()
	: buffer(new uint8_t[BUFFER_LENGTH]), bi(0), isEscaped(false), rPrevID(0), rLength(0), rID(0), rCHK(0), isReceiveBusy(false), collected(Ring<Frame>()),
	  transmitBufferLength(0), transmitBuffer(0), transmitterState(IDLE), responseByte(0), transmitID(1), doTransmitResponse(false), isTransmissionIdle(true)
	{}

uint16_t EndPoint::generateChecksum(const Frame& frame){
	uint16_t chk = 0;

	chk += frame.id;

	for (int i = 0; i < frame.length; i++){
		chk += frame.message[i];
	}
	return chk;
}

void EndPoint::processCollected(){

	Frame frame = collected.remove();

	if (frame.length > 0){

		uint16_t chk = generateChecksum(frame);

		if (chk == frame.chk){

			rPrevID = frame.id;
			transmitACK();
			received.add(frame);

		} else {

			transmitNAK();
			delete frame.message;

		}

	}

}

uint8_t EndPoint::receive(uint8_t** out){

	// empty frame initializes members to zero
	Frame frame = received.remove();
	*out = frame.message;
	return frame.length;
}

uint8_t EndPoint::fetchPacketToSend(uint8_t** out){

	uint8_t len = 0;
	uint8_t* buffer = 0;

	if (doTransmitResponse){

		len = 1;
		buffer = &responseByte;
		doTransmitResponse = false;

	} else if (transmitterState == SENDING){

		len = transmitBufferLength;
		buffer = transmitBuffer;
		transmitterState = WAIT_ACK;
	}

	// if no frame to send the callback chain stalled, next sender must restart it
	isTransmissionIdle = len == 0;

	*out = buffer;
	return len;
}

void EndPoint::resetReceiver(){
	bi = 0;
	isEscaped = false;
	rLength = 0;
	rID = 0;
	rCHK = 0;
	isReceiveBusy = false;
}


void EndPoint::transmitNAK(){
	responseByte = NAK;
	doTransmitResponse = true;
	if (isTransmissionIdle){
		initiateTransmission();
	}
}

void EndPoint::transmitACK(){
	responseByte = ACK;
	doTransmitResponse = true;
	if (isTransmissionIdle){
		initiateTransmission();
	}
}

void EndPoint::transmitFrame(){
	if (transmitterState == IDLE){

		Frame frame;

		// fetch frame from sendable (BUT THIS WILL RUN IN AN INTERRUPT)
		if (!sendable.isInterruptRemoveSafe()){
			// TODO: frame transmission will stall if nothing is done here (will restart when new frame will be sent or in case of reconnection)
			return;
		}

		frame = sendable.remove();

		if (frame.message != nullptr){

			transmitBuffer = frame.message;
			transmitBufferLength = frame.length;
			transmitterState = SENDING;
		}
	}

	if (isTransmissionIdle){
		initiateTransmission();
	}
}

void EndPoint::retransmitFrame(){
	transmitterState = SENDING;
	if (isTransmissionIdle){
		initiateTransmission();
	}
}

void EndPoint::processEncodable(){

	Frame frame = encodable.remove();

	if (frame.message == nullptr){
		return;
	}

	Frame dummy;
	dummy.message = frame.message + 4;
	dummy.length = frame.length;
	dummy.id = transmitID;

	transmitID++;
	if (transmitID >= MAX_ID){
		transmitID = 1;
	}

	uint16_t chk = generateChecksum(dummy);

	// Let frame length be the buffer length, message length is extracted
	uint8_t length = frame.length;
	frame.length += frame.chk + 6;

	// Extra length if checksum need to be escaped
	if (IS_ESCABABLE(chk & 0xFF)) frame.length++;
	if (IS_ESCABABLE((chk >> 8) & 0xFF)) frame.length++;

	// Store calculated chk
	frame.chk = chk;

	// Store the received ID
	frame.id = dummy.id;

	// Prepare frame fields
	frame.message[0] = SOF;
	frame.message[1] = (~length) & 0xFF;
	frame.message[2] = frame.message[1];
	frame.message[3] = frame.id;
	frame.message[length + 4] = (chk >> 8) & 0xFF;
	frame.message[length + 5] = (chk) & 0xFF;

	// Escape message body and checksum
	uint8_t* to = frame.message + frame.length - 1; // points to end of frame buffer, escape characters and extra fields are added
	uint8_t* from = frame.message + length + 6; // points to the end of message body which now includes checksum

	// move every byte to its place and do escaping
	while (from > frame.message + 4){
		from--;
		if (IS_ESCABABLE(*from)){
			*to-- = DO_ESCAPE(*from);
			*to-- = ESCAPE;
		} else {
			*to-- = *from;
		}
	}

	// ready to send frame
	sendable.add(frame);
	transmitFrame();
}

void EndPoint::send(uint8_t length, uint8_t* message){
	Frame frame;

	for(int i = 0; i < length; i++){
		if (IS_ESCABABLE(message[i])){
			frame.chk++;
		}
	}

	frame.length = length;

	// Frame CHK field stores the number of escapable characters in message until preparation
	// Plus 2 bytes added for possible escape of checksum
	frame.message = new uint8_t[length + frame.chk + 6 + 2];

	memcpy(frame.message + 4, message, length);

	encodable.add(frame);

	signalEncodableFrame();
}

void EndPoint::rx(uint8_t data){

	// buffer contents:
	// 0	  1		 2	  3					   LEN+3		LEN+4
	// nLEN | nLEN | ID | ... DATA x LEN ... | CHK & 0xFF | (CHK & 0xFF00) >> 8


	if (data == SOF){ // process message frame start

		resetReceiver();
		isReceiveBusy = true;

	} else if(isReceiveBusy){ // process message frame body

		buffer[bi++] = data;

		if (bi < 2){

		} else if (bi == 2){ // process nLEN pair

			rLength = ~buffer[bi - 1];
			if (buffer[bi - 2] == buffer[bi - 1] && rLength > 0 && rLength <= MAX_FRAME_LENGTH){

			} else {
				resetReceiver();
				transmitNAK();
			}

		} else if (bi == 3){ // process ID

			if (buffer[bi - 1] > 0 && buffer[bi - 1] <= MAX_ID && buffer[bi - 1] != rPrevID){
				rID = buffer[bi - 1];
			} else {
				resetReceiver();
				transmitNAK();
			}

		} else { // process body

			if (!isEscaped && data == ESCAPE){
				// valid escape character
				isEscaped = true;
				bi--;

			} else {

				if (IS_ESCABABLE(data)){
					// non escaped control character in body
					resetReceiver();
					transmitNAK();

				} else if (isEscaped){
					// de-escape character

					buffer[bi - 1] = DE_ESCAPE(data);
				} else {
					// regular byte is stored
				}

				if (bi == rLength + 5){ // if last byte in body then extract CHK and pass frame to validation
					rCHK = (buffer[bi - 2] << 8) | buffer[bi - 1];

					uint8_t* msg = new uint8_t[rLength];

					// copy message bytes
					// +3 for skipping nLEN pair and ID in the head of the buffer
					memcpy(msg,buffer + 3,rLength);

					Frame frame(rLength, rID, msg, rCHK);
					collected.add(frame);
					resetReceiver();
					signalCollectedFrame();
				}
			}

		}

	} else { // process ACK and NAK bytes

		if (transmitterState == WAIT_ACK){
			if (data == ACK){

				// frame is received by the other endpoint, release sent frame and fetch new
				transmitBufferLength = 0;
				delete transmitBuffer;
				transmitBuffer = 0;
				transmitterState = IDLE;
				transmitFrame();

			} else if (data == NAK){

				// frame is rejected by the other endpoint, resend it
				retransmitFrame();
			}
		}
	}
}

