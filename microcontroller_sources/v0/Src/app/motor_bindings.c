/*
 * motor_bindings.c
 *
 *  Created on: 2018. jan. 27.
 *      Author: Projekt
 */

#include <stdint.h>
#include "stm32f3xx_hal.h"
#include "tim.h"

volatile char isSteppingActive = 0;

TIM_OC_InitTypeDef CoilPWMConfig = {TIM_OCMODE_PWM1, 0, TIM_OCPOLARITY_HIGH, TIM_OCFAST_DISABLE, TIM_OCIDLESTATE_RESET};

void initMotionPeripheral(uint16_t timdiv){

	// Configure PWM frequency
	HAL_TIM_Base_Stop(&htim1);
	htim1.Init.CounterMode =  TIM_COUNTERMODE_UP;
	htim1.Init.Period = 128;	// ARR register
	HAL_TIM_Base_Init(&htim1);

	TIM_OC_InitTypeDef sConfigOC;
	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = 32;		// PWM duty cycle
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	sConfigOC.OCIdleState = TIM_OCIDLESTATE_SET;

	HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_1);
	HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_1);

	HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);
	HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_2);

	HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);
	HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_3);

	HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_4);
	HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4);
	HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_4);

	htim17.Init.Period = 700;
	htim17.Init.Prescaler = timdiv - 1;
	HAL_TIM_Base_Init(&htim17);

	// Start interrupt for reloading PWM duty cycles
	HAL_TIM_Base_Start_IT(&htim17);
}

void actuateX(int16_t coilA, int16_t coilB){

	if (coilA < 0){
		HAL_GPIO_WritePin(M1CA_PH_GPIO_Port, M1CA_PH_Pin, GPIO_PIN_SET);
		coilA = -coilA;
	} else {
		HAL_GPIO_WritePin(M1CA_PH_GPIO_Port, M1CA_PH_Pin, GPIO_PIN_RESET);
	}

	if (coilB < 0){
		HAL_GPIO_WritePin(M1CB_PH_GPIO_Port, M1CB_PH_Pin, GPIO_PIN_SET);
		coilB = -coilB;
	} else {
		HAL_GPIO_WritePin(M1CB_PH_GPIO_Port, M1CB_PH_Pin, GPIO_PIN_RESET);
	}


	// TIM1 CH1 is Motor 1 Coil B
	CoilPWMConfig.Pulse = coilB;
	HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_1);
	HAL_TIM_PWM_ConfigChannel(&htim1, &CoilPWMConfig, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_1);

	// TIM1 CH2 is Motor 1 Coil A
	CoilPWMConfig.Pulse = coilA;
	HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);
	HAL_TIM_PWM_ConfigChannel(&htim1, &CoilPWMConfig, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_2);

}

void actuateY(int16_t coilA, int16_t coilB){

	if (coilA < 0){
		HAL_GPIO_WritePin(M2CA_PH_GPIO_Port, M2CA_PH_Pin, GPIO_PIN_SET);
		coilA = -coilA;
	} else {
		HAL_GPIO_WritePin(M2CA_PH_GPIO_Port, M2CA_PH_Pin, GPIO_PIN_RESET);
	}

	if (coilB < 0){
		HAL_GPIO_WritePin(M2CB_PH_GPIO_Port, M2CB_PH_Pin, GPIO_PIN_SET);
		coilB = -coilB;
	} else {
		HAL_GPIO_WritePin(M2CB_PH_GPIO_Port, M2CB_PH_Pin, GPIO_PIN_RESET);
	}

	// TIM1 CH3 is Motor 2 Coil A
	CoilPWMConfig.Pulse = coilA;
	HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);
	HAL_TIM_PWM_ConfigChannel(&htim1, &CoilPWMConfig, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_3);

	// TIM1 CH4 is Motor 2 Coil B
	CoilPWMConfig.Pulse = coilB;
	HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_4);
	HAL_TIM_PWM_ConfigChannel(&htim1, &CoilPWMConfig, TIM_CHANNEL_4);
	HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_4);
}

void actuateZ(int16_t coilA, int16_t coilB){

}

void setMicrostepLength(uint16_t ticks){
	//HAL_TIM_Base_Stop_IT(&htim17);
	htim17.Init.Period = ticks;
	//HAL_TIM_Base_Init(&htim17);
	//HAL_TIM_Base_Start_IT(&htim17);

	htim17.Instance->ARR = ticks;
}
