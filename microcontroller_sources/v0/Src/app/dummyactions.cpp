/*
 * dummyactions.cpp
 *
 *  Created on: 2017. dec. 30.
 *      Author: Projekt
 */

#include <app/systemstates.h>

void IdleAction(){

}

void SettingsAction(){

}

void ExecutionAction(){

}

bool SettingsToIdleTransition(){
	return false;
}

bool IdleToSettingsTransition(){
	return false;
}

bool ExecutionToIdleTransition(){
	return false;
}

bool IdleToExecutionTransition(){
	return false;
}
