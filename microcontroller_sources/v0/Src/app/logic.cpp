
#include <app/systemstates.h>
#include <app/frameprotocol.h>
#include <app/scheduler.h>
#include "app/stepping.h"
#include "app/motor_bindings.h"
#include "main.h"
#include "stm32f3xx_hal.h"
#include "usart.h"
#include "tim.h"


#define UART2_BUFFER_LENGTH 140

Scheduler<void(*)()> sch;
EndPoint ep;
uint8_t uart2Buffer[UART2_BUFFER_LENGTH];


void delay(){
	HAL_Delay(1000);
}

// Callback for scheduler
void processCollectedFrame(){
	ep.processCollected();
}

// Callback for scheduler
void processEncodableFrame(){
	ep.processEncodable();
}

// Frame protocol binding
void EndPoint::signalCollectedFrame(){
	sch.addSingle(processCollectedFrame);
}

// Frame protocol binding
void EndPoint::signalEncodableFrame(){
	sch.addSingle(processEncodableFrame);
}

// Frame protocol binding
void EndPoint::initiateTransmission(){
	uint8_t* frame;
	uint8_t len = fetchPacketToSend(&frame);
	if (len > 0){
		HAL_UART_Transmit_DMA(&huart2,frame,len);
	}
}

extern "C" void HAL_UART_TxCpltCallback(UART_HandleTypeDef* huart){
	if (huart == &huart2){
		ep.initiateTransmission();
	}
}

extern "C" void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart){

	if (huart == &huart2){
		uint16_t length = UART2_BUFFER_LENGTH - huart->hdmarx->Instance->CNDTR;

		for (int i = 0; i < length; i++){
			ep.rx(uart2Buffer[i]);
		}

		// USART2 RX CHANNEL
		// Reenable DMA after receiver idle interrupt.
		// This is necessary to generate transfer complete flag on DMA after a packet is received.
		HAL_UART_Receive_DMA(&huart2, uart2Buffer, UART2_BUFFER_LENGTH);
	}
}

void stepping(){
	if (HAL_GPIO_ReadPin(B1_GPIO_Port,B1_Pin) == GPIO_PIN_RESET){

		HAL_GPIO_WritePin(LD2_GPIO_Port,LD2_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin(M_EN_GPIO_Port,M_EN_Pin,GPIO_PIN_SET);

		const int stepPerSec = 500; // 200 is half turn, full turn wont be accurate
		const int tickPerStep = 1000 / stepPerSec;
		static int state = 0;
		static uint32_t lastTick = 0;

		uint32_t tick = HAL_GetTick();

		if (lastTick + tickPerStep <= tick){

			lastTick = tick;

			switch(state){
			case 0:
				state++;
				HAL_GPIO_WritePin(M1CA_EN_GPIO_Port,M1CA_EN_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(M1CA_PH_GPIO_Port,M1CA_PH_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M1CB_EN_GPIO_Port,M1CB_EN_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M1CB_PH_GPIO_Port,M1CB_PH_Pin,GPIO_PIN_RESET);

				HAL_GPIO_WritePin(M2CA_EN_GPIO_Port,M2CA_EN_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(M2CA_PH_GPIO_Port,M2CA_PH_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M2CB_EN_GPIO_Port,M2CB_EN_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M2CB_PH_GPIO_Port,M2CB_PH_Pin,GPIO_PIN_RESET);
				break;
			case 1:
				state++;
				HAL_GPIO_WritePin(M1CA_EN_GPIO_Port,M1CA_EN_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M1CA_PH_GPIO_Port,M1CA_PH_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M1CB_EN_GPIO_Port,M1CB_EN_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(M1CB_PH_GPIO_Port,M1CB_PH_Pin,GPIO_PIN_RESET);

				HAL_GPIO_WritePin(M2CA_EN_GPIO_Port,M2CA_EN_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M2CA_PH_GPIO_Port,M2CA_PH_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M2CB_EN_GPIO_Port,M2CB_EN_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(M2CB_PH_GPIO_Port,M2CB_PH_Pin,GPIO_PIN_RESET);
				break;
			case 2:
				state++;
				HAL_GPIO_WritePin(M1CA_EN_GPIO_Port,M1CA_EN_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(M1CA_PH_GPIO_Port,M1CA_PH_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(M1CB_EN_GPIO_Port,M1CB_EN_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M1CB_PH_GPIO_Port,M1CB_PH_Pin,GPIO_PIN_RESET);

				HAL_GPIO_WritePin(M2CA_EN_GPIO_Port,M2CA_EN_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(M2CA_PH_GPIO_Port,M2CA_PH_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(M2CB_EN_GPIO_Port,M2CB_EN_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M2CB_PH_GPIO_Port,M2CB_PH_Pin,GPIO_PIN_RESET);
				break;
			case 3:
				state = 0;
				HAL_GPIO_WritePin(M1CA_EN_GPIO_Port,M1CA_EN_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M1CA_PH_GPIO_Port,M1CA_PH_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M1CB_EN_GPIO_Port,M1CB_EN_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(M1CB_PH_GPIO_Port,M1CB_PH_Pin,GPIO_PIN_SET);

				HAL_GPIO_WritePin(M2CA_EN_GPIO_Port,M2CA_EN_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M2CA_PH_GPIO_Port,M2CA_PH_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(M2CB_EN_GPIO_Port,M2CB_EN_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(M2CB_PH_GPIO_Port,M2CB_PH_Pin,GPIO_PIN_SET);
				break;
			}
		}


	} else {
		HAL_GPIO_WritePin(LD2_GPIO_Port,LD2_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(M_EN_GPIO_Port,M_EN_Pin,GPIO_PIN_RESET);

		HAL_GPIO_WritePin(M1CA_EN_GPIO_Port,M1CA_EN_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(M1CA_PH_GPIO_Port,M1CA_PH_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(M1CB_EN_GPIO_Port,M1CB_EN_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(M1CB_PH_GPIO_Port,M1CB_PH_Pin,GPIO_PIN_RESET);

		HAL_GPIO_WritePin(M2CA_EN_GPIO_Port,M2CA_EN_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(M2CA_PH_GPIO_Port,M2CA_PH_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(M2CB_EN_GPIO_Port,M2CB_EN_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(M2CB_PH_GPIO_Port,M2CB_PH_Pin,GPIO_PIN_RESET);
	}
}

void enableMoveWhenBtnPushed(){
	int value = HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin);

	isSteppingActive = value == GPIO_PIN_RESET;

	if (isSteppingActive){
		//HAL_GPIO_WritePin(LD2_GPIO_Port,LD2_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin(M_EN_GPIO_Port,M_EN_Pin,GPIO_PIN_SET);
	} else {
		//HAL_GPIO_WritePin(LD2_GPIO_Port,LD2_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(M_EN_GPIO_Port,M_EN_Pin,GPIO_PIN_RESET);

		/*
		static uint8_t s = 0;
		s = 1-s;
		HAL_GPIO_WritePin(M2CB_PH_GPIO_Port,M2CB_PH_Pin,s ? GPIO_PIN_SET : GPIO_PIN_RESET);
		HAL_GPIO_WritePin(M2CA_PH_GPIO_Port,M2CA_PH_Pin,s ? GPIO_PIN_SET : GPIO_PIN_RESET);
		*/

		static int16_t a = 1;
		static int16_t b = 127;
		static int16_t dir = -1;

		actuateX(a, b);

		dir = b == 1 || a == 1 ? -dir : dir;

		a += dir;
		b -= dir;

		HAL_Delay(5);
	}
}

extern "C" void startSchedulerSystem(){

	/*
	uint8_t rec[]={'^',0xFE,0xFE,0x01,'A','\0','B'};

	for(unsigned int i = 0; i < sizeof(rec); i++){
		uint8_t data = rec[i];
		ep.rx(data);
	}

	HAL_UART_Receive_DMA(&huart2, uart2Buffer, UART2_BUFFER_LENGTH);

	ep.send(12,(uint8_t*)"Hello world!");
	ep.send(6,(uint8_t*)"tt\\][^"); // creates an escapable checksum

	sch.add(delay);
	sch.run();
	/**/

	/*

	// pin configuration is important!!!
	sch.add(stepping);
	sch.run();

	/*/

	HAL_GPIO_WritePin(LD2_GPIO_Port,LD2_Pin,GPIO_PIN_SET);

	// TODO release eset�n az optimaliz�l�s elront valamit
	// TODO az�rt fagy le, mert t�l sok sz�m�t�s van az interrupt-ban
	initMotionPeripheral(4);

	sch.add(enableMoveWhenBtnPushed);
	sch.run();
	/**/

	while(1);
}
