/*
 * stepping.h
 *
 *  Created on: 2018. jan. 27.
 *      Author: Projekt
 */

#ifndef APP_STEPPING_H_
#define APP_STEPPING_H_

#ifdef __cplusplus
extern "C"{
#endif

enum Coordinate {X = 0, Y = 1, Z = 2};

extern volatile char isSteppingActive;


void microstepInterrupt();


#ifdef __cplusplus
}
#endif

#endif /* APP_STEPPING_H_ */
