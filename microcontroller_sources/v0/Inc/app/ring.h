/*
 * list.h
 *
 *  Created on: 2017. dec. 3.
 *      Author: Projekt
 */

#ifndef APP_RING_H_
#define APP_RING_H_

template<class T>
class Ring{

	class Item{
	public:
		T val;
		Item* next;
		bool empty;

		Item(T val, Item* next) : val(val), next(next), empty(false) {}
		Item(Item* next) : next(next), empty(true) {}
		bool isEmpty(){
			return empty;
		}
	};

	Item* first;
	Item* holder;
	Item* last;

	int cleanHead(){
		int cnt = 0;
		// remove empty items at head
		while(first->next != nullptr && first->next->isEmpty()){
			// while first item in list exists (after the empty head) and it does not have value, remove it
			Item* del = first->next;
			first->next = del->next;
			delete del;

			if (first->next == nullptr){
				last = first;
			}

			cnt++;
		}

		return cnt;
	}

public:
	Ring() : first(new Item(nullptr)), holder(nullptr), last(first) {}
	~Ring(){
		while(first != nullptr){
			Item* item = first;
			first = item->next;
			delete item;
		}
	}

	void add(T val){

		Item* item = new Item(val, nullptr);

		if (holder != nullptr){
			// an item is currently being added (code must run in interrupt to reach this point)
			// this block won't be interrupted

			// insert element between the other thread's nel and el
			// the addition will be finished by thread
			item->next = holder->next;
			holder->next = item;
		} else {
			// normal execution or no competitive action in interrupt

			// create null item for spacing
			Item* empty = new Item(item);

			// lock list by creating holder structure
			holder = empty;

			// put holder structure on top of list
			Item* prevLast = last;
			last->next = holder;
			last = item;

			// release holder structure which is now on top of the list
			holder = 0;

			// remove the empty element from the list
			// this is safe because the list is not locked
			// hence the concurrent modifications cannot interact with previously last item
			prevLast->next = (prevLast->next)->next;

			// delete the empty placeholder
			delete empty;

		}
	}

	T remove(){
		T val;

		// create holder structure
		Item* empty2 = new Item(nullptr);
		Item* empty1 = new Item(empty2);

		// lock the list with holder structure
		holder = empty1;

		while(first->next != nullptr && first->next->isEmpty()){
			// while first item in list exists (after the empty head) and it does not have value, remove it
			Item* del = first->next;
			first->next = del->next;
			delete del;
		}

		if (first->next != nullptr){
			// found a valid element, return it
			val = first->next->val;

			// remove the container element
			Item* del = first->next;
			first->next = del->next;
			delete del;
		}

		if (first->next == nullptr){
			// all elements removed, set pointers to null element
			last = first;
		}

		// put holder structure on top of the list
		Item* prevLast = last;
		prevLast->next = empty1;
		last = empty2;

		// release holder structure which is now on top of the list
		holder = 0;

		// remove empty1 from the list
		// this is safe because the list is not locked
		// hence the concurrent modifications cannot interact with previously last item
		prevLast->next = (prevLast->next)->next;
		delete empty1;

		return val;
	}

	template<class P>
	void remove_if(P predicate){

		// create holder structure
		Item* empty2 = new Item(nullptr);
		Item* empty1 = new Item(empty2);

		// lock the list with holder structure
		holder = empty1;

		Item* head = first;

		// iterate on full ring
		while(head->next != nullptr){
			Item* del = head->next;

			// if the item is empty or it has to be removed, remove it
			if (del->isEmpty() || predicate(del->val)){
				if (last == del){
					last = head;
				}
				head->next = del->next;
				delete del;
			} else {
				head = del->next;
			}

		}

		// put holder structure on top of the list
		Item* prevLast = last;
		prevLast->next = empty1;
		last = empty2;

		// release holder structure which is now on top of the list
		holder = 0;

		// remove empty1 from the list
		// this is safe because the list is not locked
		// hence the concurrent modifications cannot interact with previously last item
		prevLast->next = (prevLast->next)->next;
		delete empty1;
	}

	T& rotate(){
		T* val;

		// create holder structure
		Item* empty2 = new Item(nullptr);
		Item* empty1 = new Item(empty2);

		// lock the list with holder structure
		holder = empty1;

		int rmcnt = cleanHead();

		// place ring element to end, bring the new current item to first->next

		// if valid item was in head,
		if (rmcnt == 0){
			Item* old = first->next;
			if (last != old){
				// more then one items in ring

				// put current item to end of ring
				first->next = old->next;
				last->next = old;
				old->next = nullptr;
				last = old;

				cleanHead();

			} else {
				// only one item is in the ring, don't do anything
			}
		} else {
			// if cleanup had to be made, the next non empty element is selected
		}

		// if the ring contains anything
		if (first->next != nullptr){
			// found a valid element, return it
			val = &(first->next->val);
		}

		// put holder structure on top of the list
		Item* prevLast = last;
		prevLast->next = empty1;
		last = empty2;

		// release holder structure which is now on top of the list
		holder = 0;

		// remove empty1 from the list
		// this is safe because the list is not locked
		// hence the concurrent modifications cannot interact with previously last item
		prevLast->next = (prevLast->next)->next;
		delete empty1;

		return *val;
	}

	T& currentItem(){
		cleanHead();
		return first->next->val;
	}

	bool hasItem(){
		cleanHead();
		return first->next != nullptr;
	}

	bool isInterruptRemoveSafe(){
		return holder == nullptr;
	}


};


#endif /* APP_RING_H_ */
