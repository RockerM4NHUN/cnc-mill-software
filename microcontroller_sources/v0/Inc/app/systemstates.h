/*
 * systemstates.h
 *
 *  Created on: 2017. dec. 29.
 *      Author: Projekt
 */

#ifndef APP_SYSTEMSTATES_H_
#define APP_SYSTEMSTATES_H_

#include <app/statemachine.h>

/* Generated content */

void SettingsAction();
void IdleAction();
void ExecutionAction();

bool SettingsToIdleTransition();
bool IdleToSettingsTransition();
bool ExecutionToIdleTransition();
bool IdleToExecutionTransition();

/* Generated content end */

StateMachine generateStateMachine();



#endif /* APP_SYSTEMSTATES_H_ */
