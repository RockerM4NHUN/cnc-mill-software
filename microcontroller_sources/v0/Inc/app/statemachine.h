/*
 * statemachine.h
 *
 *  Created on: 2017. dec. 27.
 *      Author: Projekt
 */

#ifndef APP_STATEMACHINE_H_
#define APP_STATEMACHINE_H_

#include <vector>

class StateMachine{


public:
	class State;

private:

	State* initial;
	State* currentState;
	bool stopRequest;

	struct Transition{
		State* target;
		bool(*isFiring)();

		Transition() : target(nullptr), isFiring(nullptr) {}
		Transition(State* target, bool(*isFiring)()) : target(target), isFiring(isFiring) {}
	};

public:


	class State{

	public:
		void(*action)();
		std::vector<Transition> transitions;

		State(void(*action)()) : action(action), transitions(std::vector<Transition>(3)){

		}

		void addTransition(State* target, bool(*isFiring)()){
			transitions.push_back(Transition(target, isFiring));
		}
	};

	StateMachine() : initial(nullptr), currentState(nullptr), stopRequest(false) {}

	void setInitialState(State* state){
		initial = state;
	}

	void stop(){
		stopRequest = true;
	}

	void run(){
		stopRequest = false;

		while(!stopRequest){

			currentState->action();

			for(auto transition : currentState->transitions){
				if (transition.isFiring()){
					currentState = transition.target;
					break;
				}
			}
		}
	}

};



#endif /* APP_STATEMACHINE_H_ */
