/*
 * frameprotocol.h
 *
 *  Created on: 2017. dec. 9.
 *      Author: Projekt
 */

#ifndef APP_FRAMEPROTOCOL_H_
#define APP_FRAMEPROTOCOL_H_

#include <app/ring.h>
#include <stdint.h>

class EndPoint{

	enum TransmitterState {IDLE, SENDING, WAIT_ACK};

	struct Frame{

		Frame() : length(0), id(0), message(0), chk(0) {}
		Frame(uint8_t length, uint8_t id, uint8_t* message, uint16_t chk) : length(length), id(id), message(message), chk(chk) {}

		uint8_t length;
		uint8_t id;
		uint8_t* message;
		uint16_t chk;
	};

	// Variables for collecting frame
	uint8_t* buffer;
	uint8_t bi;
	bool isEscaped;

	uint8_t rPrevID;
	uint8_t rLength;
	uint8_t rID;
	uint16_t rCHK;

	bool isReceiveBusy;

	Ring<Frame> collected;
	Ring<Frame> received;
	Ring<Frame> sendable;
	Ring<Frame> encodable;

	// Variables for transmitting
	uint8_t transmitBufferLength; // length of transmit frame buffer
	uint8_t* transmitBuffer; // points to array that is being sent
	TransmitterState transmitterState; // determines if frame is being transmitted, awaiting response or transmission is inactive
	uint8_t responseByte; // if not zero ACK or NAK byte must be sent between frames
	uint8_t transmitID;

	bool doTransmitResponse; // if true response byte should be transmitted

	bool isTransmissionIdle; // if true and transmission is required, the interrupt chain should be triggered to start

	// Helper functions

	void resetReceiver();
	uint16_t generateChecksum(const Frame& frame);

	// Bind function for system, should not be implemented in frameprotocol.
	// Called when a frame is stored in the collected list.
	// The processCollected function should be called once for every signal, outside of interrupt.
	// More calls on it is handled properly.
	void signalCollectedFrame();

	// Bind function for system, should not be implemented in frameprotocol.
	// Called when a frame is stored in the encodable list.
	// The encodeFrame function should be called once for every signal, outside of interrupt.
	// More calls on it is handled properly.
	void signalEncodableFrame();

	void transmitNAK();
	void transmitACK();
	void transmitFrame();
	void retransmitFrame();

	// stores buffer (which should not be deleted by caller) to out pointer
	// returns the length of the buffer (0 means no buffer was returned)
	uint8_t fetchPacketToSend(uint8_t** out);

public:
	EndPoint();

	void rx(uint8_t data);
	void processCollected();
	void processEncodable();


	// Bind function for system, should not be implemented in frameprotocol.
	// The function has to initiate the callback chain for continuous transmission.
	// It should use the fetchPacketToSend function to get a buffer to send.
	// If a packet was fetched it must be sent.
	// When a packet is sent out, the initiateTransmission function should be called to continue transmission.
	void initiateTransmission();

	// TODO implement send
	void send(uint8_t length, uint8_t* message);
	// Az oszt�ly tartalmazzon egy list�t, amiben az elk�ldend� �zenetek vannak,
	// ha jelenleg nincs kimen� �zenet, vagy nincs kapcsol�dva a modul, akkor is induljon el a kommunik�ci�.

	uint8_t receive(uint8_t** out);
};


#endif /* APP_FRAMEPROTOCOL_H_ */
