/*
 * scheduler.h
 *
 *  Created on: 2017. dec. 3.
 *      Author: Projekt
 */

#ifndef APP_SCHEDULER_H_
#define APP_SCHEDULER_H_


/*
* scheduler.h
*
* A simple task scheduler implementation that cycles through tasks and inserts
* one shot task executions between regular ones.
*
*/

#include <app/ring.h>


template<class F>
class Scheduler{

	class MatchGroup;

public:

	class Task{

	public:

		F callback;
		int callCount;
		int group;

		Task() : callCount(0), group(0) {}

		Task(F callback) : callback(callback), callCount(0), group((int)(this)){
			// empty
		}
		Task(F callback, int group) : callback(callback), callCount(0), group(group){
			// empty
		}

		void run(){
			callCount++;
			callback();
		}
	};


	Scheduler() : activeTask(nullptr), stopRequest(false), removeActiveGroup(false), regularTasks(Ring<Task>()), oneShotTasks(Ring<Task>()) {
		// empty
	}

	void run(){

		while(!stopRequest){

			if (oneShotTasks.hasItem()){
				Task temp = oneShotTasks.remove();
				activeTask = &temp;

				activeTask->run();

				activeTask = nullptr;
			} else if(regularTasks.hasItem()) {
				activeTask = &regularTasks.currentItem();

				activeTask->run();
				regularTasks.rotate();

				activeTask = nullptr;
			}

		}

		stopRequest = false;

	}

	void stop(){
		stopRequest = true;
	}

	int add(F callback){
		Task task(callback);
		regularTasks.add(task);
		return task.group;
	}

	void addToGroup(F callback, int group){
		Task task(callback, group);
		regularTasks.add(task);
	}

	void addSingle(F callback){
		Task task(callback, 0);
		oneShotTasks.add(task);
	}

	void remove(int group){

		if (activeTask != nullptr && activeTask->group == group){
			removeActiveGroup = true;
		} else {
			regularTasks.remove_if(MatchGroup(group));
		}

	}

	const Task& currentTask() const{
		return activeTask;
	}

private:

	Task* activeTask;
	bool stopRequest;
	bool removeActiveGroup;
	Ring<Task> regularTasks;
	Ring<Task> oneShotTasks;


	class MatchGroup{
		int group;
	public:
		MatchGroup(int group) : group(group) {}

		bool operator()(const Task& task){
			return group == task.group;
		}
	};

};


#endif /* APP_SCHEDULER_H_ */
