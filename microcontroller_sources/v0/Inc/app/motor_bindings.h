/*
 * motor_bindings.h
 *
 *  Created on: 2018. jan. 27.
 *      Author: Projekt
 */

#ifndef APP_MOTOR_BINDINGS_H_
#define APP_MOTOR_BINDINGS_H_

#ifdef __cplusplus
extern "C"{
#endif


void initMotionPeripheral(uint16_t);
void actuateX(int16_t coilA, int16_t coilB);
void actuateY(int16_t coilA, int16_t coilB);
void actuateZ(int16_t coilA, int16_t coilB);
void setMicrostepLength(uint16_t ticks);


#ifdef __cplusplus
}
#endif

#endif /* APP_MOTOR_BINDINGS_H_ */
