// Rastering.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <string>
#include <iostream>
#include <fstream>
#include <array>
#include <list>

using namespace std;

const string DELIM = " ";

template<class T>
constexpr inline T sqr(T o1) {
	return o1*o1;
}


ofstream csv;


const uint8_t MICROSTEPS = 128;
const array<uint16_t, MICROSTEPS> sineRise = {};
const array<uint16_t, MICROSTEPS> sineFall = {};

enum Coordinate { X = 0, Y = 1, Z = 2 };

struct Motor {

	typedef void(*ActuatingFunction)(bool sign, uint16_t amplitude);

	Motor(ActuatingFunction actuate) : machinePosition(0), phase(0), actuate(actuate) {}
	int32_t machinePosition;	// step count from machine origin

	int8_t phase;				// bipolar stepper phase

	ActuatingFunction actuate;
};

void actuateX(bool sign, uint16_t amplitude) {

}

void actuateY(bool sign, uint16_t amplitude) {

}

void actuateZ(bool sign, uint16_t amplitude) {

}

uint8_t microstep;	// current microstep
array<Motor, 3> motors = {
	Motor(actuateX),
	Motor(actuateY),
	Motor(actuateZ)
};



void step(array<Motor*, 3> motors, array<int8_t, 3> directions, uint16_t ticks) {
	// schedule timer interrupts such that the microsteps tick count will add up to "ticks", even if it is not divisible

	// save coordinates for visualization

	for (uint8_t i = 0; i < motors.size(); i++) {
		if (directions[i] != 0) {
			motors[i]->machinePosition += directions[i];

			motors[i]->phase += directions[i];
			motors[i]->phase &= 0x03;
		}
	}

	microstep = 0;

	csv << ::motors[X].machinePosition << DELIM
		<< ::motors[Y].machinePosition << DELIM
		<< ::motors[Z].machinePosition << endl;
}

struct Segment;

bool doLinearStep(Segment& s);
bool doSlowArcStep(Segment& s);
bool doFastArcStep(Segment& s);

struct Segment {

	enum Type {
		LINEAR,					// straight path
		SLOW_ARC,				// arc path, where circular motion is faster than translational
		FAST_ARC				// arc path, where circular motion is slower than translational
	};

	typedef bool(*StepFunction)(Segment&);



	/*********************** LINEAR ***********************/

	static Segment linear(array<Motor*, 3> motors, array<int32_t, 3> distance, double speed, double acceleration) {
		Segment s(LINEAR, doLinearStep, motors, distance, speed, acceleration);

		s.initializeLinear();

		return s;
	}

	Segment(Type type, StepFunction doStep, array<Motor*, 3> motors, array<int32_t, 3> distance, double speed, double acceleration)
		: type(type), doStep(doStep), motors(motors), distance(distance), speed(speed), acceleration(acceleration) {}

	void inline initializeLinear() {

		// transform distance vector to the positive quadrant
		for (uint8_t i = 0; i < distance.size(); i++) {
			direction[i] = distance[i] > 0 ? 1 : (distance[i] < 0 ? -1 : 0);
			distance[i] = distance[i] < 0 ? -distance[i] : distance[i];
		}

		// select dominant axis
		if (distance[X] < distance[Y] || distance[X] < distance[Z]) {
			// x is not dominant, swap needed
			if (distance[Y] > distance[Z]) {
				// y is dominant
				swap(motors[X], motors[Y]);
				swap(distance[X], distance[Y]);
				swap(direction[X], direction[Y]);
			}
			else {
				// z is dominant
				swap(motors[X], motors[Z]);
				swap(distance[X], distance[Z]);
				swap(direction[X], direction[Z]);
			}
		}

		// calculate slope for secondary axes
		dy = abs(((double)(distance[Y])) / ((double)(distance[X])));
		dz = abs(((double)(distance[Z])) / ((double)(distance[X])));

		// initialize errors (starting point is on raster hence initial errors must be zero)
		errory = 0;
		errorz = 0;
	}



	/*********************** ARC ***********************/

	/*
		Arc segment with all the angles to the contained points in the interval of [k*45� ; (k+1)*45�], k is integer.
		Arc is defined by three points and (redundantly) the radius. X and Y will produce the arc, Z has linear movement.
		The axes should define a right handed coordinate system in reality.

		start: relative position from the origin, on the raster
		distance: vector from start to end, both on raster
	 */
	static Segment arc(array<Motor*, 3> motors, array<int32_t, 3> distance, double speed, double acceleration, array<int32_t, 2> start, double radius) {
		
		if (abs(distance[Z]) > abs(distance[X]) && abs(distance[Z]) > abs(distance[Y])) {
			Segment s(FAST_ARC, doFastArcStep, motors, distance, speed, acceleration, start, radius);
			s.initializeFastArc();
			return s;
		} else {
			Segment s(SLOW_ARC, doSlowArcStep, motors, distance, speed, acceleration, start, radius);
			s.initializeSlowArc();
			return s;
		}
	}

	Segment(Type type, StepFunction doStep, array<Motor*, 3> motors, array<int32_t, 3> distance, double speed, double acceleration, array<int32_t, 2> start, double radius)
		: type(type), doStep(doStep), motors(motors), distance(distance), speed(speed), acceleration(acceleration), start(start), radiusSqr(radius*radius) {}

	void inline initializeSlowArc() {
		
		// transform distance vector to the positive quadrant
		for (uint8_t i = 0; i < distance.size(); i++) {
			direction[i] = distance[i] > 0 ? 1 : (distance[i] < 0 ? -1 : 0);
			distance[i] = distance[i] < 0 ? -distance[i] : distance[i];
		}

		// select dominant axis
		if (distance[Y] > distance[X]) {
			// y is dominant
			swap(motors[X], motors[Y]);
			swap(distance[X], distance[Y]);
			swap(direction[X], direction[Y]);
			swap(start[X],start[Y]);
		}

		// calculate slope for linear axis
		dz = abs(((double)(distance[Z])) / ((double)(distance[X])));

		// initialize linear error (starting point is on raster hence initial error must be zero)
		errorz = 0;
	}

	void inline initializeFastArc() {

		// transform distance vector to the positive quadrant
		for (uint8_t i = 0; i < distance.size(); i++) {
			direction[i] = distance[i] > 0 ? 1 : (distance[i] < 0 ? -1 : 0);
			distance[i] = distance[i] < 0 ? -distance[i] : distance[i];
		}

		// select dominant axis
		if (distance[Y] > distance[X]) {
			// y is dominant
			swap(motors[X], motors[Y]);
			swap(distance[X], distance[Y]);
			swap(direction[X], direction[Y]);
			swap(start[X], start[Y]);
		}

		// calculate slope for linear axis
		dx = abs(((double)(distance[X])) / ((double)(distance[Z])));

		// initialize linear error (starting point is on raster hence initial error must be zero)
		errorx = 0;
	}






	/*********************** Data ***********************/

	array<Motor*, 3> motors;	// hold motor handles, it will be rearranged for rasterizer simplicity
	array<int32_t, 3> distance;	// distance in steps from the target location
	array<int8_t, 3> direction;	// direction of section on axis (+1/0/-1)

	Type type;
	StepFunction doStep;		// initialize step sequence, returns true if segment cannot make more steps (finished)

	double speed;				// speed in eucledian space, step/tick
	double acceleration; 		// acceleration in eucledian space, step/tick^2

	// Data for LINEAR
	double dy;
	double dz;
	double errory;
	double errorz;

	// Data for ARC
	array<int32_t, 2> start;
	double radiusSqr;
	double dx;
	double errorx;
};

bool doLinearStep(Segment& s) {
	if (s.distance[X] == 0) {
		return false;
	}

	array<int8_t, 3> d;

	s.errory += s.dy;
	s.errorz += s.dz;

	s.distance[X]--;

	int numOfSteppedDirections = 1;
	d[X] = s.direction[X];
	d[Y] = 0;
	d[Z] = 0;

	if (s.errory >= 0.5) {
		numOfSteppedDirections++;
		s.distance[Y]--;
		s.errory -= 1;
		d[Y] = s.direction[Y];
	}

	if (s.errorz >= 0.5) {
		numOfSteppedDirections++;
		s.distance[Z]--;
		s.errorz -= 1;
		d[Z] = s.direction[Z];
	}

	static const double timeFactor[3]{ 1, sqrt(2), sqrt(3) };

	double time = timeFactor[numOfSteppedDirections - 1] / s.speed;
	s.speed += s.acceleration;

	uint16_t ticks = time;

	// Actuate motors
	step(s.motors, d, ticks);

	return true;
}

bool doSlowArcStep(Segment& s) {
	if (s.distance[X] == 0) {
		return false;
	}

	array<int8_t, 3> d;


	// arc movement
	double errorx = abs(sqr(s.start[X] + s.direction[X]) + sqr(s.start[Y]) - s.radiusSqr);
	double errorxy = abs(sqr(s.start[X] + s.direction[X]) + sqr(s.start[Y] + s.direction[Y]) - s.radiusSqr);

	int numOfSteppedDirections = 1;

	s.start[X] += s.direction[X];
	d[X] = s.direction[X];
	s.distance[X]--;

	d[Y] = 0;

	if (errorxy < errorx) {
		// step on Y axis too
		numOfSteppedDirections++;
		s.start[Y] += s.direction[Y];
		s.distance[Y]--;
		d[Y] = s.direction[Y];
	}

	s.errorz += s.dz;
	d[Z] = 0;

	// linear movement
	if (s.errorz >= 0.5) {
		numOfSteppedDirections++;
		s.distance[Z]--;
		s.errorz -= 1;
		d[Z] = s.direction[Z];
	}

	static const double timeFactor[3]{ 1, sqrt(2), sqrt(3) };

	double time = timeFactor[numOfSteppedDirections - 1] / s.speed;
	s.speed += s.acceleration;

	uint16_t ticks = time;

	// Actuate motors
	step(s.motors, d, ticks);

	return true;
}

bool doFastArcStep(Segment& s) {

	if (s.distance[Z] == 0) {
		return false;
	}

	array<int8_t, 3> d;

	d[X] = 0;
	d[Y] = 0;

	int numOfSteppedDirections = 1;
	d[Z] = s.direction[Z];
	s.distance[Z]--;

	s.errorx += s.dx;

	// linear movement
	if (s.errorx >= 0.5) {
		numOfSteppedDirections++;
		s.distance[X]--;
		s.errorx -= 1;
		d[X] = s.direction[X];
		s.start[X] += s.direction[X];

		// arc movement
		double errorx = abs(sqr(s.start[X]) + sqr(s.start[Y]) - s.radiusSqr);
		double errorxy = abs(sqr(s.start[X]) + sqr(s.start[Y] + s.direction[Y]) - s.radiusSqr);

		if (errorxy < errorx) {
			// step on Y axis too
			numOfSteppedDirections++;
			s.start[Y] += s.direction[Y];
			s.distance[Y]--;
			d[Y] = s.direction[Y];
		}
	}

	static const double timeFactor[3]{ 1, sqrt(2), sqrt(3) };

	double time = timeFactor[numOfSteppedDirections - 1] / s.speed;
	s.speed += s.acceleration;

	uint16_t ticks = time;

	// Actuate motors
	step(s.motors, d, ticks);

	return true;
}




// Test run

list<Segment> segments;
#define PI (3.14159265359)

void generateHelix(array<Motor*, 3> motors, double radius, uint32_t rounds, int32_t elevation) {

	array<int32_t, 2> current = {0, -radius};
	array<int32_t, 2> next = {0, 0};
	int32_t elevPer8th = elevation / 8;

	for (uint32_t i = 0; i < rounds; i++) {
		for (double angle = -PI/4; angle <= 3 * PI / 2; angle += PI/4) {

			next[X] = radius*cos(angle);
			next[Y] = radius*sin(angle);

			segments.push_back(Segment::arc(motors, {next[X]-current[X], next[Y]-current[Y], elevPer8th}, 1, 0, current, radius));

			current[X] = next[X];
			current[Y] = next[Y];

		}
	}
}

void generateSegments() {
	/*
	segments.push_back(Segment::linear({ &motors[0], &motors[1], &motors[2] }, { 100,50,50 }, 1, 0));
	segments.push_back(Segment::linear({ &motors[0], &motors[1], &motors[2] }, { -100, 50, -50 }, 1, 0));
	segments.push_back(Segment::linear({ &motors[0], &motors[1], &motors[2] }, { 0,-100,50 }, 1, 0));
	*/

	/*
	// arc parameters
	array<int32_t, 2> from = { 70, 13 };
	array<int32_t, 2> to = { 30, 2 };
	array<int32_t, 2> origin = { 103, -185 };

	segments.push_back(Segment::linear({ &motors[0], &motors[1], &motors[2] }, { from[X],from[X],0 }, 1, 0));
	segments.push_back(Segment::linear({ &motors[0], &motors[1], &motors[2] }, { 0, from[Y]-from[X], 0 }, 1, 0));

	segments.push_back(Segment::arc({ &motors[0], &motors[1], &motors[2] }, { to[X]-from[X], to[Y]-from[Y], 0 }, 1, 0, { from[X]-origin[X], from[Y]-origin[Y]}, 200));
	*/

	int radius = 100;

	segments.push_back(Segment::linear({ &motors[0], &motors[1], &motors[2] }, { 10,10, 100 }, 1, 0));
	segments.push_back(Segment::linear({ &motors[0], &motors[1], &motors[2] }, { 50-10, 50-radius-10, 0 }, 1, 0));

	generateHelix({ &motors[0], &motors[1], &motors[2] }, radius, 1, -8*50);
}

int main()
{
	generateSegments();

	const string file = "C:\\Users\\Projekt\\Documents\\MATLAB\\rastering\\path.csv";
	csv.open(file, ofstream::out);

	// initial position
	csv << ::motors[X].machinePosition << DELIM
		<< ::motors[Y].machinePosition << DELIM
		<< ::motors[Z].machinePosition << endl;

	for (Segment& segment : segments) {

		cout << "New segment" << endl;

		while (segment.doStep(segment)) {

		}
	}

	csv.close();

    return 0;
}

